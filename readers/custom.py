#!/usr/bin/python3

import numpy as np
from keras.utils import Sequence


class ClassifDataReader(Sequence):
    """ Class that combined two EmbeddedReader readers in order to
    return their data in parallel in (x, y) pairs. It also returns the data
    in batches.
    The first set of data is expected to be a sequence of (embedded) text,
    while the second set is a one-hot embedded class.
    """
    def __init__(self):
        self.reader_x = None
        self.reader_y = None
        self.counter = 0
        self.batch_size = 1
        self.sentence_length = -1

    def add_readers(self, reader_x, reader_y):
        """ Adds a pair of (x, y) readers to be returned by this one.

        Parameters
        ----------
        reader_x : list(EmbeddedReader)
            A Reader with data for the X variable.
        reader_y : list(EmbeddedReader)
            A Readers with data for the Y variable.
        """
        assert len(reader_x) == len(reader_y),\
            "Readers X and Y are not properly paired"
        self.reader_x = reader_x
        self.reader_y = reader_y

    def set_batching_params(self, batch_size, sentence_length):
        """ Defines the parameters for batching the input data.

        Parameters
        ----------
        batch_size : int
            Number of sentences/words/characters that will be returned in
            every batch.
        sentence_length : int
            Length of a sentence in a batch. Shorter data points will be padded,
            while longer data points will be truncated.
        """
        self.batch_size = batch_size
        self.sentence_length = sentence_length
        self.counter = 0

    def __iter__(self):
        self.counter = 0
        return self

    def __len__(self):
        return len(self.reader_x)//self.batch_size

    def __next__(self):
        if self.counter < len(self.reader_x)//self.batch_size:
            self.counter += 1
            return self.__getitem__(self.counter-1)
        else:
            raise StopIteration

    def __getitem__(self, item):
        if item < len(self.reader_x)//self.batch_size:
            # return self.reader_x[item], self.reader_y[item]
            x = []
            y = []
            for i in range(self.batch_size):
                idx = (item * self.batch_size) + i
                x.append(self.reader_x[idx])
                y.append(self.reader_y[idx])
            pad = self.reader_x.VAL_PAD
            eos = self.reader_x.VAL_EOS
            to_return = self.pad_x(x, pad, eos)
            left = np.array(to_return, dtype=float)
            right = np.array(y, dtype=float)
            return left, right
        else:
            raise IndexError

    @staticmethod
    def pad_x(batch, pad, eos, trunc_size=2048):
        """ Pads a given batch to the proper sentence length.
        The "proper" sentence length is either the length contained in the
        self.sentence_length variable (which may require truncation), or the
        length of the longest sentence in the batch if
        self.sentence_length == -1. The EOS symbol is also added.

        Parameters
        ----------
        batch : list(list(list(float)))
            List of sentence. Each sentence
        pad : list(float)
            Embedding for the PAD symbol
        eos : list(float)
            Embedding for the EOS symbol
        trunc_size : int
            Size at which sentences are truncated.
            A batch of embedded sentences to be padded.

        Returns
        -------
        list(list(list(float)))
            A batch where each sentence has been either padded or truncated to
            the appropriate length
        """
        # len(x)+1: length of longest sentence + EOS
        length = min(trunc_size, max(map(lambda x: len(x)+1, batch)))

        return list(map(lambda line: line[0:length-1] +
                                     [eos] +
                                     [pad]*(length - (len(line) + 1)), batch))

    def randomize(self, group_by_length=True):
        """ Randomizes the data returned by this PairedReader. It is further
        possible to decide whether the data should be grouped by length
        afterwards.

        Parameters
        ----------
        group_by_length : bool
            If False, the data will be simply randomized. If True, the data will
            be grouped by length after the randomization. This will make the
            data less random, but will require less padding when batching
            data points together.

        Notes
        -----
        To coordinate multiple BaseReaders, the procedure is as follows:
        new_order = reader_a.randomize()
        reader_b.randomize(new_order)
        reader_c.randomize(new_order)
        """
        order = self.reader_x.randomize(group_by_length=True)
        self.reader_y.randomize(order)
