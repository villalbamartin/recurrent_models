#!/usr/bin/python3.5

import blingfire
import csv
import io
import random
import sqlite3
from collections import defaultdict, MutableMapping


class BaseReader:
    """ Class that reads a source file and provides random access to its
    contents. The file can be segmented by sentence, by line, by word, or
    by character.

    Notes
    -----
    Possible values for reading mode are "SENT" (segmented per sentence),
    "LINE" (segmented line by line), "WORD" (segmented word by word), or
    "CHAR" (segmented character by character).
    """
    def __init__(self):
        self.counter = 0
        self.document = []

    def read_dummy(self, string, mode="LINE"):
        """ Reads a text provided as a variable and stores it in memory.

        Parameters
        ----------
        string : str
            String that will be parsed.
        mode : str
            Selects the mode in which the input file will be segmented.
            See the module notes for valid values.
        """
        assert mode in ["SENT", "LINE", "WORD", "CHAR"], "Invalid read mode"
        self.counter = 0
        if mode == "SENT":
            self.document = blingfire.text_to_sentences(string).split("\n")
        elif mode == "LINE":
            self.document = string.split("\n")
        elif mode == "WORD":
            self.document = blingfire.text_to_words(string).split(' ')
        elif mode == "CHAR":
            self.document = []
            for c in string:
                self.document.append(c)

    def read_file(self, filename, mode="LINE", encoding="utf8"):
        """ Reads a text according to the given mode and stores it in memory.

        Parameters
        ----------
        filename : str
            Name of the file that will be read.
        mode : str
            Selects the mode in which the input file will be segmented.
            See the module notes for valid values.
        encoding : str
            Encoding of the file that will be read.
        """
        assert mode in ["SENT", "LINE", "WORD", "CHAR"], "Invalid read mode"
        with io.open(filename, "r", encoding=encoding) as fp:
            if mode == "SENT":
                self.document = blingfire.text_to_sentences(fp.read())\
                                         .split("\n")
            elif mode == "LINE":
                self.document = fp.read().split("\n")
            elif mode == "WORD":
                self.document = blingfire.text_to_words(fp.read()).split(' ')
            elif mode == "CHAR":
                self.document = []
                for c in fp.read():
                    self.document.append(c)

    def read_csv(self, filename, mode="LINE", row=0, separator=',',
                 encoding="utf8"):
        """ Reads a text according to the given mode and stores it in memory.

        Parameters
        ----------
        filename : str
            Name of the file that will be read.
        mode : str
            Selects the mode in which the input file will be segmented.
            See the module notes for valid values.
        row : int
            Row number of the field that should be read.
        separator : char
            Separator between fields.
        encoding : str
            Encoding of the file that will be read.

        Notes
        -----
        This reader does not support the "SENT" reading mode, because reading
        more than a field to see where does a sentence end defeats the purpose
        of using a CSV file to begin with.

        """
        assert mode in ["LINE", "WORD", "CHAR"], "Invalid read mode"
        with io.open(filename, "r", encoding=encoding) as fp:
            reader = csv.reader(fp, delimiter=separator)
            for line in reader:
                # Protection against final empty line
                if len(line) == 0:
                    continue

                newline = line[row]
                if mode == "LINE":
                    self.document.append(newline)
                elif mode == "WORD":
                    self.document += blingfire.text_to_words(newline).split(' ')
                elif mode == "CHAR":
                    for c in newline:
                        self.document.append(c)

    def normalize(self):
        """ Applies a normalization algorithm to the source data.
        """
        for i in range(len(self.document)):
            line = self.document[i]
            self.document[i] = line.lower()

    def extend_reader(self, other_reader):
        """ Extends the data contained in this Reader by incorporating all the
        data of another BaseReader.

        Parameters
        ----------
        other_reader : BaseReader
            Reader whose data will be included into this BaseReader.
        """
        self.document += list(map(lambda x: str(x), other_reader.document))

    def __iter__(self):
        self.counter = 0
        return self

    def __len__(self):
        return len(self.document)

    def __next__(self):
        if self.counter < len(self.document):
            self.counter += 1
            return self.__getitem__(self.counter-1)
        else:
            raise StopIteration

    def __getitem__(self, item):
        return self.document[item]

    def randomize(self, order=None, group_by_length=True):
        """ Randomizes the data returned by this reader. It is further possible
        to give a specific order, and to decide whether the data should
        be grouped by length afterwards.

        Parameters
        ----------
        order : list(int)
            If not None, the order in which elements must be reordered. Useful
            for coordinating the randomization along several BaseReaders.
        group_by_length : bool
            If False, the data will be simply randomized. If True, the data will
            be grouped by length after the randomization. This will make the
            data less random, but will require less padding when batching
            data points together.

        Returns
        -------
        list(int)
            A list of integers with the new order of the elements contained in
            this BaseReader. Useful for randomizing other BaseReaders in
            parallel.

        Notes
        -----
        To coordinate multiple BaseReaders, the procedure is as follows:
        new_order = reader_a.randomize()
        reader_b.randomize(new_order)
        reader_c.randomize(new_order)
        """
        assert not group_by_length or order is None, \
            "Grouping by order is only allowed for true randomization"
        if order is None:
            sort_order = list(range(len(self.document)))
            random.shuffle(sort_order)
        else:
            sort_order = order.copy()

        assert len(sort_order) == len(self.document),\
            "The given randomization order doesn't have the right size"

        self.document = [self.document[i] for i in sort_order]

        if group_by_length:
            temp = sorted(zip(self.document, sort_order),
                          key=lambda x: len(x[0]))
            self.document = list(map(lambda x: x[0], temp))
            sort_order = list(map(lambda x: x[1], temp))
        return sort_order


class SQLiteDict(MutableMapping):
    def __init__(self):
        super(SQLiteDict, None).__init__()
        self.db = None
        self.conn = None
        self.cursor = None
        self.default_val = None
        self.embedding_dim = None

    def open_db(self, filename):
        self.db = filename
        # I can set check_same_thread to False here only because I'm not writing
        # to the database
        self.conn = sqlite3.connect(filename, check_same_thread=False)
        self.cursor = self.conn.cursor()

    def set_embedding_dim(self, dim):
        """ Sets the dimension that the embeddings are expected to have. Values
        will be padded with 0s to reach the desired dimension.

        Parameters
        ----------
        dim : int
            Expected dimension that the embeddings should have.
        """
        self.embedding_dim = dim

    def set_default_val(self, val):
        """ Sets the value that will be returned when there's no embedding
        for a given word.

        Parameters
        ----------
        val : list(float)
            Value that is to be returned for unknown words

        """
        self.default_val = val

    def __getitem__(self, key):
        return self.get(key, self.default_val)

    def __setitem__(self, key, value):
        raise NotImplementedError("Function not needed at the moment")

    def __delitem__(self, key):
        raise NotImplementedError("Function not needed at the moment")

    def __contains__(self, key):
        query = "SELECT * FROM embeddings WHERE word = ? LIMIT 1"
        params = (key,)
        self.cursor.execute(query, params)
        return self.cursor.fetchone() is not None

    def get(self, key, default=None):
        query = "SELECT * FROM embeddings WHERE word = ? LIMIT 1"
        params = (key,)
        self.cursor.execute(query, params)
        retval = self.cursor.fetchone()
        if retval is None:
            return default
        else:
            retval = [0.0] * (self.embedding_dim - len(retval) + 1)\
                     + list(retval[1:])
            return retval

    def __eq__(self, other):
        return self.db == other.db

    def __len__(self):
        query = "SELECT count(*) FROM embeddings"
        self.cursor.execute(query)
        return int(self.cursor.fetchone()[0])

    def __del__(self):
        self.conn.close()

    def __iter__(self):
        raise NotImplementedError("Function not needed at the moment")

    def __keytransform__(self, key):
        return key

    def keys(self):
        # query = "SELECT word FROM embeddings"
        # retval = []
        # for row in self.cursor.execute(query):
        #     retval.append(row[0])
        # return retval
        raise NotImplementedError("You really shouldn't call this method")

    def values(self):
        # query = "SELECT * FROM embeddings"
        # retval = []
        # for row in self.cursor.execute(query):
        #     retval.append(row[1:])
        # return retval
        raise NotImplementedError("You really shouldn't call this method")

    def items(self):
        # query = "SELECT * FROM embeddings"
        # retval = []
        # for row in self.cursor.execute(query):
        #     retval.append((row[0], row[1:]))
        # return retval
        raise NotImplementedError("You really shouldn't call this method")


class EmbeddedReader(BaseReader):
    """ Class with the same properties of a regular BaseReader, but where
    the output is returned in embedded form. """

    def __init__(self):
        super(EmbeddedReader, self).__init__()
        self.embedded_document = []
        self.embedding = None
        self.lazy = False
        self.word2val = defaultdict(lambda: 0)
        self.TOKEN_PAD = "PAD"
        self.TOKEN_UNK = "UNK"
        self.TOKEN_EOS = "EOS"
        self.VAL_PAD = None
        self.VAL_UNK = None
        self.VAL_EOS = None
        self.TOKENS = [self.TOKEN_PAD, self.TOKEN_UNK, self.TOKEN_EOS]

    def read_dummy(self, string, mode="LINE"):
        old_index = len(self.embedded_document)
        super().read_dummy(string, mode)
        self.__embed_range(old_index)

    def read_file(self, filename, mode="LINE", encoding="utf8"):
        old_index = len(self.embedded_document)
        super().read_file(filename, mode, encoding)
        self.__embed_range(old_index)

    def read_csv(self, filename, mode="LINE", row=0, separator=',',
                 encoding="utf8"):
        old_index = len(self.embedded_document)
        super().read_csv(filename, mode, row, separator, encoding)
        self.__embed_range(old_index)

    def normalize(self):
        """ Applies a normalization algorithm to the source data.
        """
        super().normalize()
        self.__embed_range(0)

    def extend_reader(self, other_reader):
        super().extend_reader(other_reader)
        self.__embed_range(0)

    def __iter__(self):
        self.counter = 0
        return self

    def __len__(self):
        return len(self.embedded_document)

    def __next__(self):
        if self.counter < len(self.document):
            self.counter += 1
            return self.__getitem__(self.counter-1)
        else:
            raise StopIteration

    def __getitem__(self, item):
        if self.lazy:
            return self.__embed_sentence(item)
        else:
            return self.embedded_document[item]

    def apply_ft_embeddings(self, filename, is_database, lazy, unknown_value,
                            dim=300):
        """ Applies embeddings to the data from the selected reader. These
        embeddings can come either from an embeddings map or from a database.
        They can also be precalculated at once, or generated on demand.

        Parameters
        ----------
        filename : str
            Path to the file containing the source of the embeddings.
        is_database : bool
            If true, the given file is assumes to be a database. Otherwise,
            it is assumed to be a fastText dictionary of embeddings.
        lazy : bool
            If true, data will only be embedded when read. Otherwise, all
            embeddings will be precalculated at once
        unknown_value : list(float)
            Representation for the UNK token.
        dim : int
            Dimension of the embeddings. For fastText, it's typically 300.

        Notes
        -----
        lazy=True is useful for systems with a lot of memory, as iterating over
        the data will be faster. lazy=False is recommended for systems with
        relatively little memory, since memory usage won't explode.
        """
        self.lazy = lazy
        self.embedding = "fastText"

        # Special tokens are supposed to be a one-hot encoding plus a list of
        # 0s, except for PAD (which is all 0s) and UNK (which is treated as a
        # regular word, and therefore starts with 0). This leaves us only with
        # one token, "EOS", which is therefore represented with a single 1. To
        # add special tokens, you would change VAL_EOS to [1.0, 0.0] and make
        # your new token start with [0.0, 1.0]
        self.VAL_PAD = [0.0] * (1 + len(unknown_value))
        self.VAL_EOS = [1.0] * (1 + len(unknown_value))
        self.VAL_UNK = [0.0] + unknown_value
        prefix = [0.0] * (len(self.TOKENS) - 2)

        if is_database:
            self.word2val = SQLiteDict()
            self.word2val.open_db(filename)
            self.word2val.set_embedding_dim(len(prefix) + dim)
            self.word2val.set_default_val(self.VAL_UNK)
        else:
            fp = io.open(filename, 'r', encoding='utf-8', newline='\n',
                         errors='ignore')
            n, dim = map(int, fp.readline().split())
            # The following steps are very unintuitive:
            # * "Prefix" is the number of 0s that need to be added to a FT
            #   vector. It is the number of special tokens besides PAD and UNK
            # * VAL_PAD and VAL_EOS have specific values that I can compute now
            # * VAL_UNK has its own value. In short, it is an average of all
            #   other word vectors. Since that takes a bit to compute, I store
            #   that vector down in the code.
            self.VAL_PAD += [0.0] * dim
            self.VAL_EOS += [0.0] * dim
            for line in fp:
                tokens = line.rstrip().split(' ')
                word = tokens[0]
                vector = list(map(float, tokens[1:]))
                self.word2val[word] = prefix + vector
            fp.close()
        # Calculate embeddings for the document, if it applies
        self.__embed_range(0)

    def apply_onehot_embeddings(self, values_are_int, classes=-1):
        """ Embeds the data in this reader with a one-hot schema.

        Parameters
        ----------
        values_are_int : bool
            If True, the values that will be embedded will be treated as
            integers, guaranteeing that "2" will be one-hot encoded as
            [0, 0, 1, 0, ...]. If False, then values are added to a dictionary
            and no order is respected.
        classes : int
            The number of different classes. Useful when combined with
            values_are_int to coordinate with other possible readers.
        """
        self.embedding = "one-hot"
        all_vals = set(self.document)
        if classes > 0:
            num_zeros = classes
        else:
            num_zeros = len(all_vals)

        if values_are_int:
            for word in all_vals:
                before = [0.0] * int(word)
                after = [0.0] * (num_zeros-int(word)-1)
                self.word2val[word] = before + [1.0] + after
        else:
            indices = dict([(y, x) for x, y in enumerate(sorted(all_vals))])
            for word in all_vals:
                before = [0.0] * indices[word]
                after = [0.0] * (num_zeros - indices[word] - 1)
                self.word2val[word] = before + [1.0] + after
        self.word2val.default_factory = lambda: [0.0] * num_zeros
        self.__embed_range(0)

    def apply_int_embeddings(self):
        """ Converts the given data to integers.
        """
        self.embedding = "integers"
        self.__embed_range(0)

    def __embed_range(self, start):
        """ Creates the embedded representation of the internal document from
        a certain index on.

        Parameters
        ----------
        start : int
            Index from which to start the embedding. It will fill the
            variable self.embedded_document from this index to
            len(self.document).
        """
        if self.embedding is None:
            # Nothing can be embedded if we haven't loaded embeddings
            return

        if self.lazy:
            missing_sents = (len(self.document) - len(self.embedded_document))
            self.embedded_document += [None] * missing_sents
        else:
            if self.embedding == "fastText":
                new_emb = []
                for sent in self.document[start:]:
                    new_emb.append(list(map(lambda x: self.word2val[x],
                                            blingfire.text_to_words(sent).
                                            split(' '))))
            elif self.embedding == "one-hot":
                new_emb = list(map(lambda x: self.word2val[x],
                                   self.document[start:]))
            elif self.embedding == "integers":
                new_emb = list(map(lambda x: int(x), self.document[start:]))
            self.embedded_document = self.embedded_document[:start] + new_emb

        assert len(self.document) == len(self.embedded_document),\
            "Some elements have not been embedded properly"

    def __embed_sentence(self, index):
        if self.embedding is None:
            raise NotImplementedError("Cannot embed a sentence "
                                      "without embeddings")
        elif self.embedding == "fastText":
            list_of_words = blingfire.text_to_words(self.document[index])\
                                     .split(' ')
            retval = []
            for word in list_of_words:
                tmp_var = self.word2val[word]
                retval.append(tmp_var)
            return retval
        elif self.embedding == "one-hot":
            return self.word2val[self.document[index]]
        elif self.embedding == "integers":
            return int(self.document[index])
        else:
            raise NotImplementedError("Invalid embedding type")

    def randomize(self, order=None, group_by_length=True):
        raise NotImplementedError("I still haven't made it to here")

    @staticmethod
    def default_emb_en():
        """ Returns a pre-calculated average of all 1M fastText English vectors.
        This has been reported to be a good representation for the 'UNK' token.
        """
        return [-0.000117, -0.002454, -0.000389, 0.000810, 0.000176, -0.006009,
                -0.002139, -0.021483, -0.001675, -0.000725, 0.002675, -0.002633,
                -0.004696, -0.001430, 0.000231, -0.002212, 0.008567, -0.000514,
                0.016054, -2.786506, 7.850047, 0.000772, 9.073344, 0.014891,
                0.003121, -0.000691, -0.002272, 0.000861, 0.014317, -0.000193,
                0.003635, -0.000603, 0.000541, 0.016608, -0.001190, -0.000651,
                0.004784, -0.000506, 0.003656, 0.002240, 0.000291, -0.004532,
                -0.002322, -0.001327, 0.001782, 0.004135, 0.001810, 0.000436,
                -0.003265, -0.002032, -0.001689, -0.00130, -0.001578, -0.000649,
                -0.000311, -0.000403, -0.000332, -0.001871, -0.012433, 0.002497,
                0.000152, -0.003836, 0.004606, -0.000675, 0.002620, 0.000351,
                0.000625, 0.000626, 0.000240, -0.003977, -0.000429, -0.004549,
                0.013452, 0.000189, 5.588463, -0.000404, -0.002228, -0.013650,
                0.000758, -0.000616, -0.000776, 0.000708, -0.005048, -0.000991,
                0.003429, -0.001943, 0.002160, -0.000977, -0.002194, -0.000546,
                0.001579, -0.005301, -0.021129, -0.008919, -0.000626, 0.038280,
                0.001488, -0.000838, 0.000544, 0.001218, 0.001392, 0.000262,
                -0.001689, -0.001055, -0.005880, -0.032557, -0.001286, 0.001285,
                0.002094, 0.005122, -0.000109, 0.009042, 0.015807, 0.000687,
                0.000334, 0.000690, 0.002670, 0.000810, 0.020329, 0.000704,
                -0.000270, 0.002497, -0.000675, 0.000525, -0.001474, 0.000709,
                0.004394, -0.009268, -0.000651, 0.019051, 0.000563, 0.006027,
                -0.002635, -0.002654, -0.002767, 0.000829, 0.000129, 0.009120,
                -0.003419, -0.002924, -0.004088, 1.141106, 0.001525, 0.001143,
                -0.003055, -0.002985, 0.000647, 0.000405, -0.002782, -0.003096,
                -0.000703, 0.002942, 0.002544, -0.002467, 0.003627, -0.000134,
                0.000416, 0.001690, 0.001160, 0.002528, -0.002834, 0.001179,
                -0.007314, 0.001528, -0.001922, -4.568417, -0.002622, -0.001076,
                -0.001650, 0.001737, 0.000530, -0.000770, -0.001178, 0.003371,
                -0.001179, 0.006988, -0.000932, -0.001671, -0.009039, -0.001513,
                0.003801, 8.569371, -0.002696, 0.003252, -0.000239, 0.007052,
                -0.000298, 0.051370, -0.000558, 0.001458, 0.003082, 0.013730,
                -0.013397, 0.002051, 0.000678, 0.001858, -0.000839, -6.628579,
                0.007687, 0.001585, -0.023357, 0.016679, -0.001308, 0.002405,
                0.000812, 0.000458, 0.000130, 0.002486, -0.004516, -0.021031,
                0.039135, -0.005625, 0.001657, -0.001429, 0.017778, 0.000283,
                -0.003186, 0.000217, -0.000577, -0.002205, 0.000924, -0.001588,
                -0.001298, 0.024570, 0.009526, 0.003416, 0.003848, 0.025846,
                0.001007, -0.004017, 0.000337, -0.021289, -0.008040, -0.001779,
                0.004995, -0.001114, 0.001132, -0.001864, 0.001783, -0.000335,
                0.013613, 0.000380, -0.000173, -0.003619, 0.018252, 0.002795,
                -0.002510, -0.001443, -0.002008, -0.002249, 1.298197, -0.000776,
                -0.003235, 0.001409, 0.002482, -0.000829, 0.003301, 0.002160,
                0.001977, -0.002037, -0.002089, 0.002376, 0.001122, -0.009034,
                0.000495, -0.001592, -0.000464, -0.000898, -0.003551, -0.000595,
                0.000953, 0.030841, -0.018321, 0.003177, 0.005212, -0.015390,
                -5.903625, 0.001983, 0.000403, -0.001764, -0.004679, 0.000701,
                0.001940, 0.000238, -0.004736, 0.000214, 0.001600, -0.001159,
                0.000113, 6.696380, -0.001521, 0.000835, 0.002203, -0.003406,
                0.001262, 0.000417, 0.002890, -0.000748, 0.001123, 0.001255]
