#!/usr/bin/python3.5

import numpy as np
import keras.backend as K
from models.attention import AttentionModel
from readers.base import EmbeddedReader
from readers.custom import ClassifDataReader

if __name__ == '__main__':
    # Read training data
    mytext = ("Miss Morstan entered the room with a firm step\n"
              "and an outward composure of manner.\n"
              "She was a blonde young lady, small, dainty, well gloved,\n"
              "and dressed in the most perfect taste.\n"
              "There was, however, a plainness and simplicity about her costume\n"
              "which bore with it a suggestion of limited means.\n"
              "The dress was a sombre grayish beige, untrimmed and unbraided,\n"
              "and she wore a small turban of the same dull hue,\n"
              "relieved only by a suspicion of white feather in the side.\n"
              "Her face had neither regularity of feature nor beauty\n"
              "of complexion, but her expression was sweet and amiable,\n"
              "and her large blue eyes were singularly spiritual and sympathetic."
              )
    mytext_val = ("In an experience of women which extends over many nations\n"
                  "and three separate continents, I have never looked upon a face\n"
                  "which gave a clearer promise of a refined and sensitive nature.\n"
                  "I could not but observe that as she took the seat\n"
                  "which Sherlock Holmes placed for her, her lip trembled,\n"
                  "her hand quivered, and she showed every sign of intense inward agitation.")

    # Task: predict if the word "and" is present
    myclass = "0 1 0 1 0 0 0 1 0 0 1 1"
    myclass_valid = "1 1 1 0 0 1"

    # Read X and Y data
    reader_x = EmbeddedReader()
    reader_x.read_dummy(mytext, mode="LINE")
    reader_x.apply_ft_embeddings(filename="data/database.sqlite",
                                 is_database=True,
                                 lazy=True,
                                 unknown_value=EmbeddedReader.default_emb_en())
    reader_y = EmbeddedReader()
    reader_y.read_dummy(myclass, mode="WORD")
    reader_y.apply_int_embeddings()

    reader = ClassifDataReader()
    reader.set_batching_params(batch_size=3, sentence_length=16)
    reader.add_readers(reader_x, reader_y)

    # Read validation data
    reader_x_val = EmbeddedReader()
    reader_x_val.read_dummy(mytext_val, mode="LINE")
    reader_x_val.apply_ft_embeddings(filename="data/database.sqlite",
                                     is_database=True,
                                     lazy=True,
                                     unknown_value=EmbeddedReader.default_emb_en())
    reader_y_val = EmbeddedReader()
    reader_y_val.read_dummy(myclass_valid, mode="WORD")
    reader_y_val.apply_int_embeddings()

    reader_val = ClassifDataReader()
    reader_val.set_batching_params(batch_size=3, sentence_length=16)
    reader_val.add_readers(reader_x_val, reader_y_val)

    # Train a model
    mymodel = AttentionModel()
    mymodel.build_model(sentence_length=7, embedding_dim=301, hidden_dim=301)
    mymodel.train(10, reader, reader_val)

    #results, attn_weights = mymodel.predict(np.ones((6, 7, 3)))
    # # How to plot attention
    # print(retval[1][0].tolist())

