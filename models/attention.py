#!/usr/bin/python3.5

import keras.backend as K
import numpy as np
from keras import regularizers
from keras.layers import Dense, Dropout, Input, LSTM, Dot, Permute, Flatten
from keras.layers.wrappers import Bidirectional
from keras.models import Model
from keras_self_attention import SeqSelfAttention
from models.abstract import AbstractModel


class AttentionModel(AbstractModel):
    """ Implementation of an attention model. The model attempts to implement
    the model as detailed in the paper.

    References
    ----------
    Lin et al., 2017. A Structured Self-Attentive Sentence Embedding. ICLR 2017.
    """
    def __init__(self):
        super(AttentionModel, self).__init__()
        self.input = None
        self.annotation = None
        self.model = None

    def build_model(self, sentence_length, embedding_dim, hidden_dim,
                    dropout=0.5):
        # Part 1: encoder
        self.input = Input(shape=(sentence_length, embedding_dim))

        encoder = Bidirectional(LSTM(hidden_dim,
                                     return_sequences=True,
                                     dropout=dropout))
        attention = SeqSelfAttention(attention_activation='sigmoid',
                                     attention_width=15,
                                     attention_type=SeqSelfAttention.ATTENTION_TYPE_MUL,
                                     kernel_regularizer=regularizers.l2(1e-4),
                                     bias_regularizer=regularizers.l1(1e-4),
                                     attention_regularizer_weight=1e-4,
                                     return_attention=True
                                     )
        embedded_repr = attention(encoder(self.input))
        self.annotation = embedded_repr[1]

        # Part 2: classifier
        #flat = Flatten()
        mlp_1 = Dense(2000, activation='relu')
        mlp_2 = Dense(1, activation='sigmoid')
        #self.output = mlp_2(mlp_1(flat(embedded_repr[0])))
        self.output = mlp_2(mlp_1(embedded_repr[0]))

        self.model = Model(inputs=[self.input], outputs=[self.output, self.annotation])
        self.model.compile(optimizer="rmsprop", loss='binary_crossentropy')

    def penalization(self):
        """ Implements the penalization term described in Section 2.2.

        Returns
        -------
        float
            A penalization term for the current annotation matrix
        """
        perm = Permute((2, 1))
        prod = Dot(axes=(2, 1))([self.annotation, perm(self.annotation)])
        id = np.eye(K.int_shape(prod)[1])
        return np.linalg.norm(prod - id)**2

    def train(self, epochs, train_reader, valid_reader):
        self.model.fit_generator(train_reader, epochs=epochs,
                                 validation_data=valid_reader)

    def evaluate(self, test_reader):
        pass

    def predict(self, reader):
        result = self.model.predict(reader)
        # https://github.com/lumiqai/UOI-1806.01264/blob/master/model.py
        #print(result[0].shape)
        #print(result[1].shape)
        return result

    def save_model(self, filename):
        pass

    def load_model(self, filename):
        pass
