#!/usr/bin/python3.5

from readers.custom import ClassifDataReader


class AbstractModel:
    """ Abstract implementation of a keras model.
    """

    def __init__(self):
        pass

    def build_model(self, sentence_length, embedding_dim, hidden_dim,
                    dropout=0.5):
        """ Builds the model.

        Parameters
        ----------
        sentence_length : int
            Length of a sentence. If set to "None", the sentences
            are expected to have varying length across batches.
        embedding_dim : int
            Dimension of the embeddings for the words of the input sentence.
        hidden_dim : int
            Dimension of the hidden layers.
        dropout : float
            Probability of any given hidden cell being set to 0 during training.
            Useful to prevent overfitting.
        """
        raise NotImplementedError("You need to implement this function")

    def train(self, train_reader, valid_reader):
        """ Trains a model using the given training and validation data.

        Parameters
        ----------
        train_reader : ClassifDataReader
            Reader containing the training data
        valid_reader : ClassifDataReader
            Reader containing the validation data

        Returns
        -------

        """
        raise NotImplementedError("You need to implement this function")

    def evaluate(self, test_reader):
        """ Evaluates the accuracy of a given model.

        Parameters
        ----------
        test_reader : ClassifDataReader
            Reader containing the test data

        Returns
        -------
        float
            The accuracy of the model when evaluated over the given data.
        """
        raise NotImplementedError("You need to implement this function")

    def predict(self, reader):
        """ Generates predictions for the given data

        Parameters
        ----------
        reader : ??
            Reader containing the data that will be predicted

        Returns
        -------
        """
        raise NotImplementedError("You need to implement this function")

    def save_model(self, filename):
        """ Saves the weights of a model in the given file.

        Parameters
        ----------
        filename : str
            File where the weights of the model will be saved.
        """
        raise NotImplementedError("You need to implement this function")

    def load_model(self, filename):
        """ Loads the weights of a model stored in the given file.

        Parameters
        ----------
        filename : str
            File where the weights of the model are stored
        """
        raise NotImplementedError("You need to implement this function")
