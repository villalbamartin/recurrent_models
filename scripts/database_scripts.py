#!/usr/bin/python3

import sqlite3
import io


def ft_file_to_database(embeddings_file, database_file):
    """ Migrates a file containing official fastText embeddings to an
    SQLite database.

    Parameters
    ----------
    embeddings_file : str
        File containing an official distribution of fastText embeddings.
    database_file : str
        File where the SQLite database will be written.
    """
    # Create a database
    conn = sqlite3.connect(database_file)
    c = conn.cursor()
    query = ("CREATE TABLE IF NOT EXISTS embeddings ("
             "word TEXT NOT NULL PRIMARY KEY")
    for i in range(300):
        query += ", V{:03d} float".format(i)
    query += ")"
    c.execute(query)

    # Read the embeddings following the official steps
    # and insert them into the database
    query = "INSERT INTO embeddings VALUES (" + ", ".join(["?"]*301) + ")"
    fin = io.open(embeddings_file, 'r', encoding='utf-8', newline='\n',
                  errors='ignore')
    n, d = map(int, fin.readline().split())
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        # data[tokens[0]] = map(float, tokens[1:])
        c.execute(query, (tokens))
    conn.commit()
    conn.close()


def ft_avg_vector(embeddings_file):
    """ Calculates the average value of the embeddings

    Parameters
    ----------
    embeddings_file : str
        File containing an official distribution of fastText embeddings.
    """
    # Read the embeddings following the official steps
    # and calculate their average
    fin = io.open(embeddings_file, 'r', encoding='utf-8', newline='\n',
                  errors='ignore')
    n, d = map(int, fin.readline().split())
    avg = [0.0] * d
    count = 1
    data = {}
    for line in fin:
        tokens = line.rstrip().split(' ')
        # data[tokens[0]] = map(float, tokens[1:])
        # New average = old average * (n-1)/n + new value /n
        avg = [x[0]*((count-1)/count) + float(x[1])/count
               for x in zip(avg, tokens[1:])]
        count += 1
    print(avg)
