#!/usr/bin/python3.5

from readers.base import BaseReader, EmbeddedReader
import blingfire
import os
import random
import sqlite3
import tempfile
import unittest


class TestBaseReader(unittest.TestCase):
    def setUp(self):
        self.text = ("I don't feel a thing\n"
                     "and stop remembering.\n"
                     "But days are just like moments turned to hours.\n"
                     "Mother used to say: \"if you want, you'll find your way\".\n"
                     "But mother never danced\n"
                     "through fire showers.")
        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.filename = self.fakefile.name
        self.fakefile.write(self.text)
        self.fakefile.close()

        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.csvfile = self.fakefile.name
        counter = 0
        for line in self.text.split("\n"):
            print("{}\t{}".format(counter, line), file=self.fakefile)
            counter += 1
        self.fakefile.close()

    def tearDown(self):
        if os.path.isfile(self.filename):
            os.unlink(self.filename)
        if os.path.isfile(self.csvfile):
            os.unlink(self.csvfile)

    def test_iteration(self):
        reader = BaseReader()
        test_results = [("SENT", 4), ("LINE", 6), ("WORD", 45), ("CHAR", 193)]
        for mode, items in test_results:
            reader.read_file(self.filename, mode)
            counter = 0
            self.assertEqual(len(reader), items,
                             "'len' for {} mode is not working properly"
                             .format(mode))
            for _ in reader:
                counter += 1
            self.assertEqual(counter, items,
                             "{} mode is not being iterated properly"
                             .format(mode))

    def test_dummy_read(self):
        reader = BaseReader()
        test_results = [("SENT", 4), ("LINE", 6), ("WORD", 45), ("CHAR", 193)]
        for mode, items in test_results:
            reader.read_dummy(self.text, mode)
            counter = 0
            self.assertEqual(len(reader), items,
                             "'len' for {} mode is not working properly"
                             .format(mode))
            for _ in reader:
                counter += 1
            self.assertEqual(counter, items,
                             "{} mode is not being iterated properly"
                             .format(mode))

    def test_csv_read(self):
        # The number of characters for this text is less because the '\n'
        # character is no longer included
        test_results = [("LINE", 6), ("WORD", 45), ("CHAR", 188)]
        for mode, items in test_results:
            reader = BaseReader()
            reader.read_csv(self.csvfile, mode, row=1, separator="\t")
            counter = 0
            self.assertEqual(len(reader), items,
                             "'len' for {} mode is not working properly"
                             .format(mode))
            for _ in reader:
                counter += 1
            self.assertEqual(counter, items,
                             "{} mode is not being iterated properly"
                             .format(mode))

    def test_normalization(self):
        reader = BaseReader()
        reader.read_dummy(self.text, "WORD")
        self.assertEqual(reader[0], "I",
                         "Dummy reader is not reading text properly")
        self.assertEqual(reader[10], "But",
                         "Dummy reader is not reading text properly")
        reader.normalize()
        self.assertEqual(reader[0], "i",
                         "Dummy reader is not normalizing properly")
        self.assertEqual(reader[10], "but",
                         "Dummy reader is not normalizing properly")

    def test_extend(self):
        reader = BaseReader()
        reader.read_dummy("A B C D E F G", "WORD")
        self.assertEqual(7, len(reader),
                         "Wrong initialization of first reader")
        other_reader = BaseReader()
        other_reader.read_dummy("H I J K L", "WORD")
        self.assertEqual(5, len(other_reader),
                         "Wrong initialization of second reader")
        reader.extend_reader(other_reader)
        self.assertEqual(12, len(reader),
                         "Elements from other reader were not added correctly")
        self.assertEqual(reader[0], "A", "BaseReader[0] is the wrong element")
        self.assertEqual(reader[6], "G", "BaseReader[6] is the wrong element")
        self.assertEqual(reader[7], "H", "BaseReader[7] is the wrong element")
        self.assertEqual(reader[11], "L", "BaseReader[11] is the wrong element")

    def test_randomize(self):
        test_results = [("LINE", 6), ("WORD", 45), ("CHAR", 188)]
        reader_a = BaseReader()
        reader_b = BaseReader()
        reader_c = BaseReader()
        for mode, items in test_results:
            reader_a.read_csv(self.csvfile, mode, row=1, separator="\t")
            reader_b.read_csv(self.csvfile, mode, row=1, separator="\t")
            reader_c.read_csv(self.csvfile, mode, row=1, separator="\t")

            order = reader_a.randomize(group_by_length=False)
            reader_b.randomize(order, group_by_length=False)
            reader_c.randomize(order, group_by_length=False)
            for i in range(len(reader_a)):
                self.assertEqual(reader_a[i], reader_b[i],
                                 "Readers a and b are not in sync")
                self.assertEqual(reader_b[i], reader_c[i],
                                 "Readers b and c are not in sync")

            order = reader_a.randomize(group_by_length=True)
            reader_b.randomize(order, group_by_length=False)
            reader_c.randomize(order, group_by_length=False)
            for i in range(len(reader_a)):
                self.assertEqual(reader_a[i], reader_b[i],
                                 "Readers a and b are not in sync")
                self.assertEqual(reader_b[i], reader_c[i],
                                 "Readers b and c are not in sync")
            for i in range(1, len(reader_a)):
                self.assertTrue(len(reader_a[i-1]) <= len(reader_a[i]),
                                "reader_a is not strictly increasing")
                self.assertTrue(len(reader_b[i-1]) <= len(reader_b[i]),
                                "reader_a is not strictly increasing")
                self.assertTrue(len(reader_c[i-1]) <= len(reader_c[i]),
                                "reader_a is not strictly increasing")


class TestEmbeddedReader(unittest.TestCase):
    def setUp(self):
        self.text = ("I don't feel a thing\n"
                     "and stop remembering.\n"
                     "But days are just like moments turned to hours.\n"
                     "Mother used to say: \"if you want, you'll find your way\".\n"
                     "But mother never danced\n"
                     "through fire showers.")
        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.filename = self.fakefile.name
        self.fakefile.write(self.text)
        self.fakefile.close()

        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.csvfile = self.fakefile.name
        counter = 0
        for line in self.text.split("\n"):
            print("{}\t{}".format(counter, line), file=self.fakefile)
            counter += 1
        self.fakefile.close()

        # Create both a CSV and an SQLite database with fake embeddings
        self.embed_dim = 10
        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.sqlitefile = self.fakefile.name
        self.fakefile.close()
        self.conn = sqlite3.connect(self.sqlitefile)
        self.c = self.conn.cursor()
        query = ("CREATE TABLE IF NOT EXISTS embeddings ("
                 "word TEXT NOT NULL PRIMARY KEY")
        for i in range(10):
            query += ", V{:03d} float".format(i)
        query += ")"
        self.c.execute(query)

        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.ftfile = self.fakefile.name
        all_words = set(blingfire.text_to_words(self.text).split(' '))

        # Begin writing the results
        query = "INSERT INTO embeddings VALUES (" + ", ".join(["?"] * 11) + ")"
        print("{} {}".format(len(all_words), self.embed_dim), file=self.fakefile)
        for word in all_words:
            vector = [str(random.random()) for _ in range(self.embed_dim)]
            print("{} {}".format(word, " ".join(vector)), file=self.fakefile)
            self.c.execute(query, [word]+vector)
        self.conn.commit()
        self.conn.close()
        self.fakefile.close()

    def tearDown(self):
        if os.path.isfile(self.filename):
            os.unlink(self.filename)
        if os.path.isfile(self.csvfile):
            os.unlink(self.csvfile)
        if os.path.isfile(self.ftfile):
            os.unlink(self.ftfile)
        if os.path.isfile(self.sqlitefile):
            os.unlink(self.sqlitefile)

    def testOneHot(self):
        reader = EmbeddedReader()
        reader.read_dummy("1 2 3 2 1 2 2 3 3 1 3 1 0 0 0 0", mode="WORD")
        reader.apply_onehot_embeddings(True, 4)
        self.assertEqual(16, len(reader), "Improper number of words")
        self.assertEqual([1.0, 0.0, 0.0, 0.0], reader[15],
                         "Improper one-hot embedding of \"0\"")
        self.assertEqual([0.0, 1.0, 0.0, 0.0], reader[0],
                         "Improper one-hot embedding of \"1\"")
        self.assertEqual([0.0, 0.0, 1.0, 0.0], reader[1],
                         "Improper one-hot embedding of \"2\"")
        self.assertEqual([0.0, 0.0, 0.0, 1.0], reader[2],
                         "Improper one-hot embedding of \"3\"")

    def testFTFile(self):
        for is_db in [True, False]:
            if is_db:
                emb_file = self.sqlitefile
            else:
                emb_file = self.ftfile

            for is_lazy in [True, False]:
                reader = EmbeddedReader()
                reader.read_csv(self.csvfile, "LINE", row=1, separator="\t")
                reader.apply_ft_embeddings(emb_file, is_database=is_db,
                                           lazy=is_lazy,
                                           unknown_value=[0.0]*self.embed_dim)
                dim = -1
                for line in reader:
                    for word in line:
                        if dim == -1:
                            dim = len(word)
                        self.assertEqual(dim, len(word),
                                         "Not all embeddings have the same "
                                         "dimension (DB: {}, L:{})"
                                         .format(is_db, is_lazy))
                self.assertEqual(reader[2][0], reader[4][0],
                                 "The word \"But\" is not properly embedded"
                                 " (DB: {}, L:{})".format(is_db, is_lazy))
                self.assertNotEqual(reader[3][0], reader[4][1],
                                    "The word \"Mother\" is not properly "
                                    "embedded (DB: {}, L:{})"
                                    .format(is_db, is_lazy))
                reader.normalize()
                self.assertEqual(reader[3][0], reader[4][1],
                                 "The word \"mother\" is not properly embedded "
                                 " (DB: {}, L:{})".format(is_db, is_lazy))

    def testDBFile(self):
        reader = EmbeddedReader()
        reader.read_csv(self.csvfile, "LINE", row=1, separator="\t")
        reader.apply_ft_embeddings(self.sqlitefile, is_database=True, lazy=True,
                                   unknown_value=[0.0]*self.embed_dim, dim=10)
        dim = -1
        for line in reader:
            for word in line:
                if dim == -1:
                    dim = len(word)
                self.assertEqual(dim, len(word),
                                 "Not all embeddings have the same dimension")
        self.assertEqual(reader[2][0], reader[4][0],
                         "The word \"But\" is not properly embedded")
        self.assertNotEqual(reader[3][0], reader[4][1],
                            "The word \"Mother\" is not properly embedded")
        reader.normalize()
        self.assertEqual(reader[3][0], reader[4][1],
                         "The word \"mother\" is not properly embedded")

        reader = EmbeddedReader()
        reader.read_csv(self.csvfile, "LINE", row=1, separator="\t")
        reader.apply_ft_embeddings(self.sqlitefile, is_database=True, lazy=False,
                                   unknown_value=[0.0]*self.embed_dim, dim=10)
        dim = -1
        for line in reader:
            for word in line:
                if dim == -1:
                    dim = len(word)
                self.assertEqual(dim, len(word),
                                 "Not all embeddings have the same dimension")
        self.assertNotEqual(reader[3][0], reader[4][1],
                            "The word \"Mother\" is not properly embedded")
        reader.normalize()
        self.assertEqual(reader[3][0], reader[4][1],
                         "The word \"mother\" is not properly embedded")

"""
class TestBatchReader(unittest.TestCase):
    def setUp(self):
        self.text = ("I don't feel a thing\n"
                     "and stop remembering.\n"
                     "But days are just like moments turned to hours.\n"
                     "Mother used to say: \"if you want, you'll find your way\".\n"
                     "But mother never danced\n"
                     "through fire showers.")

    def tearDown(self):
        pass

    def test_startup(self):
        reader = BaseReader()
        reader.read_dummy(self.text, "LINE")
        batch_reader = BatchReader(reader)
        self.assertEqual(len(reader), len(batch_reader),
                         "BatchReader is not initialized properly")
        batch_reader.set_batching_params(2, -1)
        self.assertLess(len(batch_reader), len(reader),
                        "Batching is not correct")
"""

if __name__ == '__main__':
    suite_loader = unittest.TestLoader()
    suite1 = suite_loader.loadTestsFromTestCase(TestBaseReader)
    suite2 = suite_loader.loadTestsFromTestCase(TestEmbeddedReader)
    suite = unittest.TestSuite([suite1, suite2])
    unittest.TextTestRunner(verbosity=2).run(suite)
