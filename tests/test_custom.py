#!/usr/bin/python3.5

from readers.base import EmbeddedReader
from readers.custom import ClassifDataReader
import blingfire
import os
import random
import tempfile
import unittest


class TestClassifReader(unittest.TestCase):
    def setUp(self):
        self.text = ("I don't feel a thing\n"
                     "and stop remembering.\n"
                     "But days are just like moments turned to hours.\n"
                     "Mother used to say: \"if you want, you'll find your way\".\n"
                     "But mother never danced\n"
                     "through fire showers.")
        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.filename = self.fakefile.name
        self.fakefile.write(self.text)
        self.fakefile.close()

        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.csvfile = self.fakefile.name
        counter = 0
        for line in self.text.split("\n"):
            print("{}\t{}".format(counter, line), file=self.fakefile)
            counter += 1
        self.fakefile.close()

        # Create both a CSV and an SQLite database with fake embeddings
        self.embed_dim = 10
        self.fakefile = tempfile.NamedTemporaryFile("w", delete=False)
        self.ftfile = self.fakefile.name
        all_words = set(blingfire.text_to_words(self.text).split(' '))

        # Begin writing the results
        print("{} {}".format(len(all_words), self.embed_dim), file=self.fakefile)
        for word in all_words:
            vector = [str(random.random()) for _ in range(self.embed_dim)]
            print("{} {}".format(word, " ".join(vector)), file=self.fakefile)
        self.fakefile.close()

    def tearDown(self):
        if os.path.isfile(self.filename):
            os.unlink(self.filename)
        if os.path.isfile(self.csvfile):
            os.unlink(self.csvfile)
        if os.path.isfile(self.ftfile):
            os.unlink(self.ftfile)

    def testBuild(self):
        reader_x = EmbeddedReader()
        reader_x.read_file(self.filename, mode="LINE", encoding="utf8")
        reader_x.apply_ft_embeddings(self.ftfile, is_database=False, lazy=True,
                                     unknown_value=[0]*self.embed_dim,
                                     dim=self.embed_dim)
        reader_y = EmbeddedReader()
        reader_y.read_dummy("1 2 3 3 2 1", mode="WORD")
        reader_y.apply_onehot_embeddings(values_are_int=True)
        reader = ClassifDataReader()
        reader.add_readers(reader_x, reader_y)
        for i in range(1, 6):
            reader.set_batching_params(batch_size=i, sentence_length=-1)
            self.assertEqual(len(reader), 6//i, "Reader has the wrong length")

    def testWrongBuild(self):
        """ Tests that an exception is raised when the x-y data has
        different dimensions"""
        reader_x = EmbeddedReader()
        reader_x.read_file(self.filename, mode="LINE", encoding="utf8")
        reader_x.apply_ft_embeddings(self.ftfile, is_database=False, lazy=True,
                                     unknown_value=[0]*self.embed_dim,
                                     dim=self.embed_dim)
        reader_y = EmbeddedReader()
        reader_y.read_dummy("1 2 3 3 2 1 2", mode="WORD")
        reader_y.apply_onehot_embeddings(values_are_int=True)
        reader = ClassifDataReader()
        with self.assertRaises(AssertionError):
            reader.add_readers(reader_x, reader_y)

    def testPadding(self):
        """ Tests whether the padding function works as it should.
        """
        emb_0 = [0.0, 0.0]
        emb_1 = [1.0, 1.0]
        emb_2 = [2.0, 2.0]
        emb_3 = [3.0, 3.0]
        emb_pad = [0.0, 0.0]
        emb_eos = [-1.0, -1.0]
        sentence = [emb_0, emb_1, emb_2, emb_3]
        batch = [sentence] * 5

        batch_size = 5
        # Iterate over different sentence lengths, to check off-by-one errors
        for sent_len in range(1, 2+len(sentence)):
            # K iterates over every sentence in a batch, to ensure that all
            # sentences are treated equally when padding
            for k in range(0, batch_size):
                # Iterate over different sentence lengths
                retval = ClassifDataReader.pad_x(batch, emb_pad, emb_eos,
                                      trunc_size=sent_len)
                self.assertLessEqual(len(retval[k]), sent_len,
                                 "Batches have the wrong size")
                for i in range(1, len(retval)):
                    self.assertEqual(len(retval[i-1]), len(retval[i]),
                                     "Sentences in batch have different sizes")

                for i in range(0, min(sent_len-1, len(sentence))):
                    self.assertEqual(retval[k][i], sentence[i],
                                     "The sentence has been altered")
                self.assertEqual(emb_eos, retval[k][min(sent_len-1, len(sentence))],
                                 "The EOS marker is not where it should be")
                for i in range(1 + min(sent_len, len(sentence)), sent_len):
                    self.assertEqual(emb_pad, retval[k][i],
                                     "The sentence has not been padded correctly")


if __name__ == '__main__':
    suite_loader = unittest.TestLoader()
    suite1 = suite_loader.loadTestsFromTestCase(TestClassifReader)
    suite = unittest.TestSuite([suite1])
    unittest.TextTestRunner(verbosity=2).run(suite)
